### Structural Workshop - Spring 2023
Have you allways wanted to become a Soliworks expert? Maybe you are hyped for designing and you want to develop some 3D CAD skills. You may even be some random person that had nothing to do and decided to spent that free time learning how to design a CUBESAT!!!

In any case this workshop is for you as it goes through the basics of Solidworks triggering you to start designing things....and stuff! You will learn how to make a 3D CAD
model of your very own cutom 1U CubeSat designing each of its parts seperately and then put them all together to complete the assembly. You will feel the experience of 
working in Assembly mode in SOLIDWORKS and also get the chance to understand the basic principles of Modal analysis as long as interpreting post-processing results using 
ANSA META. This workshop gives the feel of the basic workflow a structural engineer in Spacedot has to go through, from the design phase to the assembly
and then the simulation. If you get stuck there is an instructional video too!
Have fun!!!
